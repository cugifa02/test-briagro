import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('https://banksystem-demoshop.herokuapp.com/')

WebUI.delay(0)

WebUI.click(findTestObject('Login/Page_DemoShop/a_Login'))

WebUI.delay(0)

WebUI.click(findTestObject('Login/Page_DemoShop/input_Email_Input.Email'))

WebUI.delay(0)

WebUI.setText(findTestObject('Login/Page_DemoShop/input_Email_Input.Email'), 'cut.gitty97@gmail.com')

WebUI.delay(0)

WebUI.click(findTestObject('Login/Page_DemoShop/input_Password_Input.Password'))

WebUI.delay(0)

WebUI.setText(findTestObject('Login/Page_DemoShop/input_Password_Input.Password'), '123Cobaa')

WebUI.delay(0)

WebUI.click(findTestObject('Login/Page_DemoShop/button_Log in'))

